package com.ketemam_yamamotn.icecream;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.net.URI;


public class Vanilla extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vanilla);

        Button vanillaButton;
        vanillaButton = (Button) findViewById(R.id.vanillaButton);

        //Gives topping options when buttons is selected
        vanillaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(view.getContext(), "1.Caramel" + "\n" + "2.Toffee" + "\n" + "3. Butter Scotch" + "\n" + "4. Pistachio Ice Cream" + "\n" + "5. Chocolate Ice Cream" + "\n" + "6. Pecan Ice Cream" + "\n" + "7. Strawberry Ice Cream" + "\n" + "8. Bananas" + "\n" + "9. Walnuts" + "\n" + "10. JUST MAKE A SUNDAE DAMMIT", Toast.LENGTH_LONG);
                toast.show();

            }
        });


        Button sundaeButton;
        sundaeButton = (Button) findViewById(R.id.sundaeButton);

        //Once sundaeButton is selected, user is taken  to the youtube page where this video
        //is found
        sundaeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/KlzVy09CvXw")));
                Log.i("Video", "Video Playing....");

            }
        });


        //Create left arrow on action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vanilla, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Returns to home page of activity
        Intent goBackToIceCreamPage = new Intent(getApplicationContext(), IceCream.class);
        startActivityForResult(goBackToIceCreamPage,0);
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        return true;

    }
}
