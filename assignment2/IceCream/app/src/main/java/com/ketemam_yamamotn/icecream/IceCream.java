package com.ketemam_yamamotn.icecream;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class IceCream extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ice_cream);

        ArrayList<String> iceCreamList = new ArrayList<String>();
        iceCreamList.add("Chocolate");
        iceCreamList.add("Vanilla");
        iceCreamList.add("Strawberry");
        iceCreamList.add("Mint Chocolate Chip");
        iceCreamList.add("Pistachio");
        iceCreamList.add("Pecan");

        ArrayAdapter<String> iceCreamAdapter =  new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, iceCreamList){

            // We re-implemented this function so that we can change the font and style of our
            // items in the listView
            public View getView(int position, View convertView, ViewGroup parent){
                TextView view = (TextView)super.getView(position,convertView,parent);
                view.setTextSize(25);
                view.setTextColor(Color.argb(1000,50,84,255));
                view.setTypeface(null, Typeface.BOLD);
                return view;
            }
        };
        ListView iceCreamView = (ListView)this.findViewById(android.R.id.list);
        iceCreamView.setAdapter(iceCreamAdapter);



        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ice_cream, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Rending about activity. Intent is instantiated if the id matches, then the About class creates
        // the activity
        if (id == R.id.action_about){
            Intent aboutActivity = new Intent(this, About.class);
            startActivity(aboutActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView parent, View v, int position, long id) {
      switch (position ){


        case 0: Intent chocActivity = new Intent(this, Chocolate.class);
           startActivity(chocActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            break;

        case 1: Intent vanillaActivity = new Intent(this, Vanilla.class);
           startActivity(vanillaActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
           break;

        case 2: Intent strawActivity = new Intent(this, Strawberry.class);
           startActivity(strawActivity);
           overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            break;

        case 3: Intent mintActivity = new Intent(this, Mint.class);
            startActivity(mintActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            break;

        case 4: Intent pistachioActivity = new Intent(this, Pistachio.class);
            startActivity(pistachioActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            break;

        case 5: Intent pecanActivity = new Intent(this, Pecan.class);
            startActivity(pecanActivity);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            break;

      }


    }
}
