package com.ketemam_yamamotn.icecream;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class Pecan extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pecan);


        Button pecanButton;
        pecanButton = (Button) findViewById(R.id.pecanButton);

        //Gives topping options when buttons is selected
        pecanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(view.getContext(), "1.Caramel" + "\n" + "2.Toffee" + "\n" + "3. Butter Scotch", Toast.LENGTH_SHORT);
                toast.show();

            }
        });

        //Creates left arrow on action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pecan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Returns to home page of activity
        Intent goBackToIceCreamPage = new Intent(getApplicationContext(), IceCream.class);
        startActivityForResult(goBackToIceCreamPage,0);
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        return true;

    }
}
