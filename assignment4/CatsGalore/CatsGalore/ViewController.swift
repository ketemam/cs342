//
//  ViewController.swift
//  CatsGalore
//
//  Created by mobiledev on 4/16/15.
//  Copyright (c) 2015 ketemam_beallej. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet weak var TableView: UITableView!
    
    @IBOutlet weak var AboutButton: UIButton!
    let cellIdentifier = "CatCells"
    let catsGalore = ["Lion", "Mad Libs", "Kitty Tinder"]

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.TableView?.delegate = self
        self.TableView?.dataSource = self
    }

    @IBAction func OnAboutPress(sender: AnyObject) {
        performSegueWithIdentifier("AboutSegue", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.catsGalore.count
    }
    

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        cell.textLabel?.text = self.catsGalore[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.None
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        if let txt = cell?.textLabel?.text{
            switch txt{
                case "Lion":
                    performSegueWithIdentifier("lionSegue", sender: self)
                case "Kitty Tinder":
                    performSegueWithIdentifier("tinderSegue", sender: self)
                default:
                    performSegueWithIdentifier("madLibsSegue", sender: self)
              
                
                
            }
            
        }
    }


}

