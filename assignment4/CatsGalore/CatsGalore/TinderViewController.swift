//
//  TinderViewController.swift
//  CatsGalore
//
//  Created by Josie Bealle on 16/04/2015.
//  Copyright (c) 2015 ketemam_beallej. All rights reserved.
//

import UIKit
class TinderViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    //list of images, from:
    //http://www.buzzfeed.com/kaelintully/dont-stop-me-now-im-havin-such-a-good-time-yea-im-a-kitten#.xn28BPWQJ
    //http://www.funnypica.com/top-30-of-most-ugliest-cats-in-the-world/
    var imglist : [String] = ["cute1.jpg", "cute2.jpg", "cute3.jpg", "cute4.jpg", "cute5.jpg", "ugly1.jpg", "ugly2.jpg", "ugly3.jpg", "ugly4.jpg", "ugly5.jpg"]
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var xButton: UIButton!
    @IBOutlet weak var heartButton: UIButton!
    
    
    //user can press buttons in addition to swiping
    @IBAction func onButtonPress(sender: UIButton) {
        if sender == self.heartButton{
            displayAlertBox("IT'S A MATCH!", message: "" )
        }
        else if sender == self.xButton{
            let randomIndex = Int(arc4random_uniform(UInt32(self.imglist.count)))
            self.profilePic.image=UIImage(named: self.imglist[randomIndex])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //randomly select image
        let randomIndex = Int(arc4random_uniform(UInt32(self.imglist.count)))
        self.profilePic.image=UIImage(named: self.imglist[randomIndex])

        //add swipe capabilites
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleSwipe(swipeGesture: UISwipeGestureRecognizer) {
        switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                displayAlertBox("IT'S A MATCH!", message: "" )
            case UISwipeGestureRecognizerDirection.Left:
                let randomIndex = Int(arc4random_uniform(UInt32(self.imglist.count)))
                self.profilePic.image=UIImage(named: self.imglist[randomIndex])
            default:
                break
            }
    }
    
    //displays match alert
    func displayAlertBox(title: String, message: String ) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "Find more Pretty Kitties!" )
        alert.show()
    }
    
   }
