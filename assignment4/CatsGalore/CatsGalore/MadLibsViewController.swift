//
//  MadLibsViewController.swift
//  CatsGalore
//
//  Created by mobiledev on 4/16/15.
//  Copyright (c) 2015 ketemam_beallej. All rights reserved.
//


import UIKit
import AVFoundation
import Foundation

class MadLibsViewController: UIViewController, UITextViewDelegate{
    
    @IBOutlet weak var nouns: UITextView!
    @IBOutlet weak var nounsPL: UITextView!
    @IBOutlet weak var adjs: UITextView!
    @IBOutlet weak var verbs: UITextView!
    @IBOutlet weak var places: UITextView!
    @IBOutlet weak var number: UITextView!
    @IBOutlet weak var activity: UITextView!
    @IBOutlet weak var tellStoryButton: UIButton!
    
    //to read story
    var synthesizer : AVSpeechSynthesizer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nouns.delegate = self
        self.nounsPL.delegate = self
        self.adjs.delegate = self
        self.verbs.delegate = self
        self.places.delegate = self
        self.number.delegate = self
        self.activity.delegate = self
        self.synthesizer = AVSpeechSynthesizer()
    }
    
    
    @IBAction func tellStory(sender: UIButton) {
        readStory()
    }
    
    func readStory(){
        //String setup
        // story source = http://en.wikipedia.org/wiki/Cat
        var catString : NSString = "The domestic NOUN1 is a small, usually furry, ADJ1, and carnivorous mammal. They are often called a housecat when kept as an indoor NOUN2, or simply a cat when there is no need to  distinguish them from other felids and felines. Cats are often valued by humans for companionship, and their ability to hunt NOUNPL1 and household pests. Cats are similar in anatomy to the other felids, with strong, flexible NOUNPL2, quick reflexes, sharp retractable NOUNPL3, and teeth adapted to killing small NOUNPL4. Cat senses fit a ADJ2 and predatory ecological niche. Cats can hear sounds too ADJ3 or too ADJ4 in frequency for human ears, such as those made by NOUN3 and other small animals. They can VERB1 in near darkness. Like most other mammals, cats have poorer color vision and a ADJ5 sense of smell than NOUNPL5. Despite being ADJ6 hunters, cats are a social NOUN4, and cat communication includes the use of a variety of vocalizations (mewing, purring, trilling, hissing, growling, and grunting), as well as cat pheromones, and types of cat-specific body language. Cats have a high breeding rate. Under controlled breeding, they can be bred and shown as registered pedigree pets, a hobby known as ACTIVITY1. Failure to VERB2 the breeding of pet cats by neutering, and the abandonment of former household pets, has resulted in large numbers of ADJ7 NOUNPL6 worldwide, requiring population control. Since cats were cult animals in ancient PLACE1, they were commonly believed to have been domesticated there, but there may have been instances of domestication as early as the Neolithic from around NUMBER1 years ago. A genetic study in 2007 concluded that domestic cats are descended from NOUN5, having diverged around 8000 BC in PLACE2. Cats are the most ADJ8 NOUN6 in the world, and are now found in almost every place where NOUNPL7 VERB3."
        let partsOfSpeech = ["NOUN", "NOUNPL", "ADJ", "VERB", "PLACE",  "NUMBER", "ACTIVITY"]

        //Get user input
        let nounlist = nouns.text.componentsSeparatedByString(",")
        let nounPLlist = nounsPL.text.componentsSeparatedByString(",")
        let adjslist = adjs.text.componentsSeparatedByString(",")
        let verbslist = verbs.text.componentsSeparatedByString(",")
        let placeslist = places.text.componentsSeparatedByString(",")
        let numberlist = number.text.componentsSeparatedByString(",")
        let activitylist = activity.text.componentsSeparatedByString(",")

        //create parts of speech box objects
        var posList = [POSbox]()
        posList.append(POSbox(name: "nouns", id: "NOUN", wordList: nounlist, countReqd: 6))
        posList.append(POSbox(name: "plural nouns", id: "NOUNPL", wordList: nounPLlist, countReqd: 7))
        posList.append(POSbox(name: "adjectives", id: "ADJ", wordList: adjslist, countReqd: 8))
        posList.append(POSbox(name: "verbs", id: "VERB", wordList: verbslist, countReqd: 3))
        posList.append(POSbox(name: "places", id: "PLACE", wordList: placeslist, countReqd: 2))
        posList.append(POSbox(name: "numbers", id: "NUMBER", wordList: numberlist, countReqd: 1))
        posList.append(POSbox(name: "activities", id: "ACTIVITY", wordList: activitylist, countReqd: 1))
        
        //alerts user on first box with wrong number of objects
        func checkUserInput() -> Bool{
            var okInput = true
            for currentPOS in posList {
                if okInput{
                    okInput = currentPOS.checkEnoughWords()
                }
            }
            return okInput
        }

        //replace placeholder text
        func doStringReplacement() -> String{
                for currentPOS in posList{
                var i = 1
                for word in currentPOS.wordList {
                    var toReplace :String = currentPOS.id+i.description
                    catString = catString.stringByReplacingOccurrencesOfString(toReplace, withString: word)
                    i+=1
                }

              
            }
            return catString as String
            
        }
        
        //read story if user input is good
        if checkUserInput(){
            let newCatString = doStringReplacement()
            var utterance = AVSpeechUtterance(string: newCatString)            
            utterance.rate = -0.4
            self.synthesizer.speakUtterance(utterance)
        }
        
    
    }
    
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        //CLEARS PLACEHOLDER TEXT
        if textView.textColor != UIColor.blackColor(){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    
        
    }
    
    //makes it shut up
    override func viewDidDisappear(animated: Bool) {
        synthesizer.pauseSpeakingAtBoundary(AVSpeechBoundary.Immediate)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//part of speech box class
class POSbox {
    
    //noun, verb...
    let name: String
    
    //words supplied by user
    let wordList :[String]
    
    //number of words required
    let countReqd: Int
    
    //placeholder id
    let id: String
    
    init(name: String, id: String, wordList: [String], countReqd: Int){
        self.name = name
        self.wordList = wordList
        self.countReqd = countReqd
        self.id = id
    }
    
    
    func checkEnoughWords() -> Bool{
        if self.wordList.count != self.countReqd {
            displayAlertBox("YOU MADE A BOOBOO", "Start by counting your "+self.name)
            return false
        }
        return true

        
    }
    
    
}

//displays alert
func displayAlertBox(title: String, message: String ) {
    let alert = UIAlertView(
        title: title,
        message: message,
        delegate: nil,
        cancelButtonTitle: "Ok" )
    alert.show()
}



