//
//  LionViewController.swift
//  CatsGalore
//
//  Created by mobiledev on 4/16/15.
//  Copyright (c) 2015 ketemam_beallej. All rights reserved.
//
//Authors: Maraki Ketema & Josie Bealle
import UIKit
import MediaPlayer

//Recieved help from Prof. Jeff Ondich in getting AutoLayout to work for the video. 


class LionViewController: UIViewController {
    @IBOutlet weak var videoView: UIView!

    // Instantiating movie player controller
    var moviePlayer : MPMoviePlayerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Defining the path at which the video is located.
        // Video taken from https://www.youtube.com/watch?v=GibiNy4d4gc
        // Lyrics taken from http://www.metrolyrics.com/circle-of-life-lyrics-lion-king.html
        let path = NSBundle.mainBundle().pathForResource("The Lion King - Circle Of Life", ofType: "mp4")
        let url = NSURL.fileURLWithPath(path!)
        self.moviePlayer = MPMoviePlayerController(contentURL: url)
        if let player = self.moviePlayer {
            
            //Defining frame size.
            player.view.frame = CGRect(x: 0, y: 0, width: self.videoView.frame.size.width, height: self.videoView.frame.size.height)
            player.view.sizeToFit()
            player.scalingMode = MPMovieScalingMode.AspectFit
            player.fullscreen = true
            player.controlStyle = MPMovieControlStyle.None
            player.movieSourceType = MPMovieSourceType.File
            player.repeatMode = MPMovieRepeatMode.One
            
            // Declaration to allow for the user of Auto Layout with player.view.
            player.view.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            //adding player view to the surrounding view
            self.videoView.addSubview(player.view)
            
            // Build the constraints.
            let viewsDictionary = ["myView":player.view]
            let myView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[myView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
            let myView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:|[myView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
            
            // Adding constraints to root view.
            self.videoView.addConstraints(myView_constraint_H)
            self.videoView.addConstraints(myView_constraint_V)
            
            player.play()
            
        }
        
        
        
    }

    override func viewDidDisappear(animated: Bool) {
        self.moviePlayer.stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

